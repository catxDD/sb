"use strict";
var express = require('express');
var router = express.Router();

function cona(url) {
    const re = new RegExp('@?(https?:)?(\/\/)?((a-zA-Z0-9.-)?[^\/]*\/)?((@)?[a-zA-Z0-9._-]*)(\/)?', 'i');
    const un = url.match(re);
    return un[5].replace('@','');
}

/* GET home page. */
router.get('/', function (req, res, next) {
    let url = req.query.url || req.query.username;
    if (url) {
        res.send('@' + cona(url));
    } else res.status(400).end();
});

module.exports = router;