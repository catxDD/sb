"use strict";
var express = require('express');
var router = express.Router();
var fetch = require('axios');
var pc = {};



getJson();
router.get('/*', function (req, res, next) {
    let json = pc;
    let ps = req.params[0];
    let p = ps.split('/').filter((e) => {
        return e;
    });
    
    if (p[0] == 'volumes') return res.json(volumes(json));
    let resp = ownProp(p);
    if(resp !== false){
        res.json(resp);
    } else {
        res.status(404).send('Not Found').end();
    }
    
});

function getJson() {
    const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
    fetch(pcUrl)
        .then((res) => {
            pc = res.data;
        })
        .catch(err => {
            console.log('Чтото пошло не так:', err);
        });
}

function volumes(obj) {
    // получаем json Object
    // Требуется вернуть hdd в формате "C:":"123213131B"
    //  hdd.volume:hdd.size
    let hdd = obj.hdd;
    let resp = {};
    for (let i in hdd) {
        let disk = hdd[i];
        if (resp[disk.volume]) {
            // если имя такого диска существует
            resp[disk.volume] += disk.size;
        } else {
            // если имени нет, то создаем его в обьекте для ответа
            resp[disk.volume] = disk.size;
        }
    }
    for (let i in resp) {
        resp[i] += 'B';
    }
    return resp;
}

function ownProp(p) {
    //функция бежит по массиву p и ищет есть ли во вложеннных обьектах
    // требуемые атрибуты
    let t = 0;
    const arr = p;
    const len = p.length - 1;
    const o = pc;
    function dig (obj,pr,l) {
        // копаем в Obj ищем внутри p если всё ок
        if (l > len) {
            return obj;
        } else {
            l++;
            if(+(Object.keys(obj).indexOf(pr)) == -1) {
                return false;
            } else {
                return dig(obj[pr],p[l],l);
            }
        }
        
    }
    if(len == -1){
        return o;
    } else return dig(o,arr[0],t);
}

module.exports = router;
