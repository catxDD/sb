"use strict";
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let a = + req.query.a || 0
  let b = + req.query.b || 0
  res.json(+a+b);
});

module.exports = router;
