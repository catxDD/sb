"use strict";
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    let fullname = req.query.fullname;
    let re = new RegExp('([0-9_\/.])','i');
    if (fullname && fullname.search(re) == -1) {
        let parts = fullname.split(' ').filter((s) => {return s});
        console.log(parts);
        switch (parts.length) {
        case 1:
            res.send(fullname);
            break;
        case 2:
            res.send(up(parts[1]) + ' ' + up(parts[0].charAt(0)) + '.');
            break;
        case 3:
            res.send(up(parts[2]) + ' ' + up(parts[0].charAt(0)) + '. ' + up(parts[1].charAt(0)) + '.');
            break;
        default:
            res.send('Invalid fullname');
        }
    } else {
        res.send('Invalid fullname');
    }
});

function up (str) {
 if(str.length > 1) {
     let fst = str[0];
     let other = str.substring(1);
     return fst.toLocaleUpperCase()+other.toLocaleLowerCase();
 } else {
     return str.toLocaleUpperCase();
 }
}

module.exports = router;
